package com.medical_system.model;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="institution")
public class Institution {
	
	public Institution(){
		
	}
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@Column(name = "name", nullable = false)
    private String name;
	
	@Column(name = "ruc", nullable = false)
    private String ruc;
	
	@Column(name = "address", nullable = false)
    private String address;
	
	@Column(name = "email", nullable = false)
    private String email;
	
	@CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;
	
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "institution")
	private User user;
}

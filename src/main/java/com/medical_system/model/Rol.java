package com.medical_system.model;




import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@Table(name="rol")
public class Rol {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String type;
	
	//@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "rol")
	//private User user;
	
	//@OneToMany(mappedBy="rol")
    //private Set<User> users;
	
	 @OneToMany(cascade = CascadeType.ALL, mappedBy = "rol")
	 private List<User> users;

	
	
	public Rol(){}
	
	public Rol(String type){
		this.type = type;
	}
	
	
}

package com.medical_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.medical_system.model.Wife;

public interface WifeRepository extends JpaRepository<Wife, Integer>{
}
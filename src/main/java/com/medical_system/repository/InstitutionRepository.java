package com.medical_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.medical_system.model.Institution;

@Repository
public interface InstitutionRepository extends JpaRepository<Institution, Long>{

}

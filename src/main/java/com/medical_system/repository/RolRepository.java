package com.medical_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.medical_system.model.Rol;


@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {


}

package com.medical_system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.medical_system.model.Husband;

public interface HusbandRepository extends JpaRepository<Husband, Integer>{
}

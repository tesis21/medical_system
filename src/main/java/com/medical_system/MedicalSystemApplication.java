package com.medical_system;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.medical_system.entity.ThemeParkRide;
import com.medical_system.model.Husband;
import com.medical_system.model.Institution;
import com.medical_system.model.Rol;
import com.medical_system.model.User;
import com.medical_system.model.Wife;
import com.medical_system.repository.HusbandRepository;
import com.medical_system.repository.InstitutionRepository;
import com.medical_system.repository.RolRepository;
import com.medical_system.repository.ThemeParkRideRepository;
import com.medical_system.repository.UserRepository;
import com.medical_system.repository.WifeRepository;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class MedicalSystemApplication implements CommandLineRunner{
	@Autowired
	InstitutionRepository institutionRepository;
	
	@Bean
	  public BCryptPasswordEncoder bCryptPasswordEncoder() {
	        return new BCryptPasswordEncoder();
	    }
	
	public static void main(String[] args) {
		SpringApplication.run(MedicalSystemApplication.class, args);
		
	}
	
	@Override
	public void run(String... arg0) throws Exception {
		//testing();
    }
	
	@Transactional
	private void testing() {
		Institution i = new Institution();  
		i.setName("name");
		i.setAddress("address");
		i.setEmail("email");
		i.setRuc("ruc");
		institutionRepository.save(i);
	}
	/*@Bean
    public CommandLineRunner sampleData(ThemeParkRideRepository repository) {
        return (args) -> {
            repository.save(new ThemeParkRide("Rollercoaster", "Train ride that speeds you along.", 5, 3));
            repository.save(new ThemeParkRide("Log flume", "Boat ride with plenty of splashes.", 3, 2));
            repository.save(new ThemeParkRide("Teacups", "Spinning ride in a giant tea-cup.", 2, 4));
        };
    }*/
	
	
	/*@EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
				.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/api/user").permitAll()
				.anyRequest().authenticated();
		}
	}*/
	
}	

package com.medical_system.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medical_system.model.Institution;
import com.medical_system.repository.InstitutionRepository;

@RestController
@RequestMapping("/api/v1")
public class InstitutionController {
	@Autowired
	InstitutionRepository  instituionRepository;
	@PostMapping("/register/institutions")
	  public Institution createInstitution(@Valid @RequestBody Institution i) {	
	    return instituionRepository.save(i);
	  }
}




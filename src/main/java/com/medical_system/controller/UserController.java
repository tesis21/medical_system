package com.medical_system.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.medical_system.exception.ResourceNotFoundException;
import com.medical_system.model.Rol;
import com.medical_system.model.User;
import com.medical_system.repository.RolRepository;
import com.medical_system.repository.UserRepository;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class UserController {
  private Long PATIENT = 2L;
  private Long DOCTOR = 1L;
  private Long STAFF = 3L;
 
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private RolRepository rolRepository;

  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;
  /**
   * Get all users list.
   *
   * @return the list
   */
  @GetMapping("/users")
  public List<User> getAllUsers() {
    return userRepository.findAll();
  }

  /**
   * Gets users by id.
   *
   * @param userId the user id
   * @return the users by id
   * @throws ResourceNotFoundException the resource not found exception
   */
  @GetMapping("/users/{id}")
  public ResponseEntity<User> getUsersById(@PathVariable(value = "id") Long userId)
      throws ResourceNotFoundException {
    User user =
        userRepository
            .findById(userId)
            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
    return ResponseEntity.ok().body(user);
  }

  /**
   * Create user user.
   *
   * @param user the user
   * @return the user
   */
  @PostMapping("/users")
  public User createUser(@Valid @RequestBody User user) {	
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    return userRepository.save(user);
  }
  
  @PostMapping("/register/doctor")
  public User createUserDoctor(@Valid @RequestBody User user) {
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    Rol rol = rolRepository.getOne(DOCTOR);
    user.setRol(rol);
    userRepository.save(user);
    return new User();
  }
  
  @PostMapping("/register/patient")
  public User createUserPatient(@Valid @RequestBody User user) {	
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    Rol rol = rolRepository.getOne(PATIENT);
    user.setRol(rol);
    userRepository.save(user);
    return new User();
  }
  
  @PostMapping("/register/staff")
  public User createUserStaff(@Valid @RequestBody User user) {	
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    Rol rol = rolRepository.getOne(STAFF);
    user.setRol(rol);
    userRepository.save(user);
    return new User();
  }

  /**
   * Update user response entity.
   *
   * @param userId the user id
   * @param userDetails the user details
   * @return the response entity
   * @throws ResourceNotFoundException the resource not found exception
   */
  @PutMapping("/users/{id}")
  public ResponseEntity<User> updateUser(
      @PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails)
      throws ResourceNotFoundException {

    User user =
        userRepository
            .findById(userId)
            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));

    user.setEmail(userDetails.getEmail());
    user.setLastName(userDetails.getLastName());
    user.setFirstName(userDetails.getFirstName());
    final User updatedUser = userRepository.save(user);
    return ResponseEntity.ok(updatedUser);
  }

  /**
   * Delete user map.
   *
   * @param userId the user id
   * @return the map
   * @throws Exception the exception
   */
  @DeleteMapping("/user/{id}")
  public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws Exception {
    User user =
        userRepository
            .findById(userId)
            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));

    userRepository.delete(user);
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }
}
